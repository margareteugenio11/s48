import AppNavbar from './components/AppNavbar'
import Courses from './pages/Courses'
import Home from './pages/Home'
import Register from './pages/Register'
import Login from './pages/Login'
import Logout from './pages/Logout'
import NotFound from './pages/NotFound'
import './App.css';

function App() {
  return (
      <Router>
      <AppNavbar/>
      <Container>
        <Switch>
          <Route exact path ="/" component = {Home} />
          <Route exact path = "/courses" component = {Courses} />
          <Route exact path = "/login" component = {Login} />
          <Route exact path = "/register" component = {Register} />
          <Route exact path = "/logout" component = {Logout} />
          <Route exact path = "*" component = {NotFound} />
        </Switch>
      </Container>
    </Router>
  );
}

export default App;

